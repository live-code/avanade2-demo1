import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HelloComponent } from './shared/hello.component';
import { AlertComponent } from './shared/alert.component';
import { PanelComponent } from './shared/panel.component';
import { UsersComponent } from './features/users/users.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/login/login.component';
import { IconGenderComponent } from './features/users/components/icon-gender.component';
import { UsersListComponent } from './features/users/components/users-list.component';
import { UsersFormComponent } from './features/users/components/users-form.component';
import { RouterModule } from '@angular/router';
import { UsersDetailsComponent } from './features/users-details/users-details.component';
import { SettingsComponent } from './features/settings/settings.component';
import { NavbarComponent } from './core/components/navbar.component';
import { UsersListItemComponent } from './features/users/components/users-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    AlertComponent,
    PanelComponent,
    UsersComponent,
    HomeComponent,
    LoginComponent,
    IconGenderComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersDetailsComponent,
    SettingsComponent,
    NavbarComponent,
    UsersListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'users', component: UsersComponent },
      { path: 'users/:id', component: UsersDetailsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'login', component: LoginComponent },
      { path: 'home', component: HomeComponent },
      { path: '', component: HomeComponent },
      { path: '**', redirectTo: 'home'},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
