export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F';
  city: string;
  birthday: number;
  bitcoin: number;
}

