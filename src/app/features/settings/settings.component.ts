import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
   
    <button (click)="themeService.theme = 'light'">light</button>
    <button (click)="themeService.theme = 'dark'">dark</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {
    console.log(themeService.theme)
  }

  ngOnInit(): void {
  }

}
