import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable( { providedIn: 'root'})
export class UsersService {
  users: User[] = [];
  error = false;
  activeUser: User;

  constructor(private http: HttpClient) {
  }

  getAll(): void {
    this.http.get<User[]>(`${environment.BASEURL}/users`)
      .subscribe(
        (result) => {
          this.users = result;
        },
        () => {
          this.error = true;
        }
      );
  }
  deleteHandler(user: User): void {
    this.error = false;

    this.http.delete(`${environment.BASEURL}/users/${user.id}`)
      .subscribe(
        () => {
          // const index = this.users.findIndex(item => item.id === user.id);
          // this.users.splice(index, 1);
          this.users = this.users.filter(item => item.id !== user.id);

          if (this.activeUser?.id === user.id) {
            this.cleanHandler();
          }
        },
        () => {
          console.log('errore!');
          this.error = true;
        }
      );

  }


  save(formData: User): void {
    if (this.activeUser?.id) {
      this.edit(formData);
    } else {
      this.add(formData);
    }
  }

  edit(formData: User): void {
    // const index = this.users.findIndex(item => item.id === this.activeUser?.id);
    // this.users[index] = { ...this.users[index], ...this.myForm.value };
    this.http.patch<User>(`${environment.BASEURL}/users/${this.activeUser.id}`, formData)
      .subscribe(result => {
        this.users = this.users.map(user => {
          return (user.id === this.activeUser?.id) ?  result : user;
        });
      });
  }

  add(formData: User): void {
    this.http.post<User>(`${environment.BASEURL}/users`, formData)
      .subscribe(result => {
        this.users = [...this.users, result];
        // this.users.push(result)
        this.cleanHandler();
      });

  }

  selectActiveHandler(user: User): void {
    this.activeUser = { ...user };
  }


  cleanHandler(): void {
    this.activeUser = { gender: null } as User;
  }
}
