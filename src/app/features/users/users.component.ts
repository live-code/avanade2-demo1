import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { User } from '../../model/user';
import { NgForm } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users',
  template: `
    <app-alert
      msg="Errore lato server"
      type="alert-info"
      *ngIf="usersService.error"
      (iconClick)="usersService.error = false"
    ></app-alert>
    
    <app-users-form
      [activeUser]="usersService.activeUser"
      (save)="usersService.save($event)"
      (clean)="usersService.cleanHandler()"
    ></app-users-form>
      
    <hr>
    <app-users-list
      [users]="usersService.users"
      [activeUser]="usersService.activeUser"
      (selectUser)="usersService.selectActiveHandler($event)"
      (deleteUser)="usersService.deleteHandler($event)"
    ></app-users-list>
  `,

})
export class UsersComponent {
  constructor(public usersService: UsersService) {
    this.usersService.cleanHandler();
    this.usersService.getAll();
  }
}
