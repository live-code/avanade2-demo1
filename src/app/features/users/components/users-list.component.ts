import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-users-list-item
      *ngFor="let user of users; let index = index;"
      [user]="user"
      [selected]="user.id === activeUser?.id"
      [index]="index"
      (deleteUser)="deleteUser.emit($event)"
      (selectUser)="selectUser.emit($event)"
    ></app-users-list-item>
    
  `,
  styles: [
  ]
})
export class UsersListComponent {
  @Input() users: User[];
  @Input() activeUser: User;
  @Output() selectUser = new EventEmitter<User>();
  @Output() deleteUser = new EventEmitter<User>();


}
