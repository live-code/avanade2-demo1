import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-list-item',
  template: `
    <div
      class="list-group-item"
      [ngClass]="{active: selected}"
      (click)="selectUser.emit(user)"
    >
      <div class="label">
        <app-icon-gender [gender]="user.gender"></app-icon-gender>
        {{index + 1}} ) {{user.name}} - {{user.age}}y - ({{user.id}})
        <div class="pull-right">
          <i class="fa fa-arrow-circle-down" (click)="isOpened = !isOpened"></i>
          <i class="fa fa-link" [routerLink]="'/users/' + user.id"></i>
          <i class="fa fa-trash" (click)="deleteUserHandler(user, $event)"></i>
        </div>
      </div>
      <div *ngIf="isOpened">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque dolores id in libero magni maxime mollitia odio recusandae saepe veritatis. Aliquam architecto cumque earum eius illo incidunt iusto, maiores sequi?
      </div>


    </div>
  `,
  styles: [
  ]
})
export class UsersListItemComponent {
  @Input() user: User;
  @Input() selected: boolean;
  @Input() index: number;
  @Output() selectUser = new EventEmitter<User>();
  @Output() deleteUser = new EventEmitter<User>();

  isOpened = false;

  deleteUserHandler(user: User, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteUser.emit(user);
  }

}
