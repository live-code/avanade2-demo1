import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { User } from '../../../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)" >
      <div *ngIf="inputName.errors?.required && f.dirty">Campo obbligatorio</div>
      <div *ngIf="inputName.errors?.minlength">Campo troppo corto</div>
      <input
        class="form-control"
        [ngClass]="{
          'is-invalid': inputName.invalid && f.dirty,
          'is-valid': inputName.valid
        }"
        type="text"
        [ngModel]="activeUser?.name"
        #inputName="ngModel" name="name" required minlength="3"
        placeholder="Name"
      >
      <input
        type="text"
        [ngModel]="activeUser?.city"
        #inputCity="ngModel"
        name="city" required
        class="form-control"
        [ngClass]="{
          'is-invalid': inputCity.invalid && f.dirty,
          'is-valid': inputCity.valid
        }"
        placeholder="City"
      >

      <select
        [ngModel]="activeUser?.gender"
        name="gender" #inputGender="ngModel" required class="form-control"
        [ngClass]="{
          'is-invalid': inputGender.invalid && f.dirty,
          'is-valid': inputGender.valid
        }"
      >
        <option [value]="null">Select a Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>
      
      <button type="submit" [disabled]="f.invalid">
        {{activeUser?.id ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="cleanHandler()" *ngIf="activeUser">ADD NEW</button>
    </form>
    
    {{render()}}

  `,
  styles: [
  ]
})
export class UsersFormComponent implements OnChanges, AfterViewInit {
  @ViewChild('f', { static: true }) myForm: NgForm;
  @Input() activeUser: User;
  @Output() save = new EventEmitter<User>();
  @Output() clean = new EventEmitter();

  cleanHandler(): void {
    this.myForm.reset();
    this.clean.emit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.activeUser?.id) {
      this.myForm.reset();
    }
  }

  ngAfterViewInit(): void {
    console.log('after view init', this.myForm)
  }

  render() {
    console.log('render form')
  }
}
