import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-icon-gender',
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-mars': gender === 'M',
        'fa-venus': gender === 'F'
      }"
    ></i>
  `,
  styles: [
  ]
})
export class IconGenderComponent {
  @Input() gender: string;
}
