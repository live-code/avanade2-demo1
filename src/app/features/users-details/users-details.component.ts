import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-users-details',
  template: `
    
    <app-alert 
      msg="user not found"
      type="alert-warning"
      *ngIf="error"
      (iconClick)="error = false"
    ></app-alert>

    <div *ngIf="data; else loader ">
      <form #f="ngForm" (submit)="save(f.value)">
        <input type="text" [ngModel]="data?.name" name="name">
        <input type="text" [ngModel]="data?.city" name="city">
        <button type="submit">SAVE</button>
      </form>
      <hr>
      
      <pre>{{data | json}}</pre>
      <button routerLink="/users">back to list</button>
    </div>
    
    <ng-template #loader>
      <div *ngIf="!error">
      LOADING
      </div>
    </ng-template>
    
  `,
  styles: [
  ]
})
export class UsersDetailsComponent {
  data: User;
  error: boolean;
  currentId: number;

  constructor(
    private activatedRouter: ActivatedRoute,
    private http: HttpClient
  ) {
    this.currentId = activatedRouter.snapshot.params.id;
    http.get<User>(`http://localhost:3000/users/${this.currentId}`)
      .pipe(
        delay(1000)
      )
      .subscribe(
        res => {
          this.data = res;
        },
        err => this.error = true
      );
  }

  // typescript utility types
  save(formData: Pick<User, 'name' | 'city'>): void {
    this.http.patch<User>('http://localhost:3000/users/' + this.currentId, formData)
      .subscribe(res => {
        this.data = res;
      });
  }
}
