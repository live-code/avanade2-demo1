import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-panel title="Profile"
               icon="fa fa-plus"
               (iconClick)="step = 1"
               *ngIf="step === 0"
    >
        <input type="text">
        <input type="text">
        <input type="text">
    </app-panel>
   
    <app-panel title="Widget"
               icon="fa fa-link"
               *ngIf="step === 1"
               (iconClick)="alertMe($event)"
    >
      <div class="row">
        <div class="col">
          <app-panel title="left">xxx</app-panel>
        </div>
        <div class="col">
          <app-panel title="right">abc</app-panel>
        </div>
      </div>
    </app-panel>
   
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  step = 0;

  constructor() { }

  ngOnInit(): void {
  }

  openUrl(url: string): void {
    window.open(url)
  }

  alertMe(status: boolean): void {
    window.alert(status)
  }

}
