
import { EventEmitter, Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-alert',
  template: `
    <div 
      class="alert"
      [ngClass]="type"
    >
      {{msg}}
      
      <div class="pull-right">
        <i class="fa fa-times" 
           (click)="iconClick.emit()"></i>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AlertComponent {
  @Input() msg: string;
  @Input() type = 'alert-danger';
  @Output() iconClick = new EventEmitter();
}
