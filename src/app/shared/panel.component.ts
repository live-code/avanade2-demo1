import { Component, Input, EventEmitter, Output } from '@angular/core';

/**
 * Questo è un toggable panel
 */
@Component({
  selector: 'app-panel',
  template: `
    <div class="card">
      <div class="card-header" (click)="isOpen = !isOpen">
        {{title}}
        
        <div class="pull-right">
          <i *ngIf="icon" [class]="icon" (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class PanelComponent {
  /**
   * Il titolo del pannello
   */
  @Input() title: string;
  /**
   * L'icona del pannello
   */
  @Input() icon: string;
  @Output() iconClick = new EventEmitter<boolean>();
  isOpen = true;

  iconClickHandler(ev: MouseEvent): void {
    ev.stopPropagation();
    this.iconClick.emit(this.isOpen);
  }
}
