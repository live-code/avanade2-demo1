
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <h1 [style.color]="color">
      <i 
        *ngIf="icon"
        [class]="icon"></i> Hello {{title}}
    </h1>
  `
})
export class HelloComponent {
  @Input() title = 'ciccio';
  @Input() color = 'red';
  @Input() icon: string;
}
