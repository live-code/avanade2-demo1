import { Injectable } from '@angular/core';

type Theme = 'dark' | 'light';

@Injectable({ providedIn: 'root'})
export class ThemeService {
  private value: Theme;

  constructor() {
    const theme = localStorage.getItem('theme') as Theme;
    this.value = theme || 'light';
  }

  set theme(theme: Theme) {
    localStorage.setItem('theme', theme)
    this.value = theme;
  }

  get theme(): Theme {
    return this.value;
  }
}
