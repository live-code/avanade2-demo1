import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      style="padding: 20px"
      [ngClass]="{
        'bg-dark text-white': themeService.theme === 'dark',
        'bg-light': themeService.theme === 'light'
      }"
    >
      <button routerLink="login">login</button>
      <button routerLink="home">home</button>
      <button routerLink="settings">settings</button>
      <button routerLink="users">users</button>
      
      theme: {{themeService.theme}}
    </div>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  constructor(public themeService: ThemeService) {
  }

  ngOnInit(): void {
  }

}
